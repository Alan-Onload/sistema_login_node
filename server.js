const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const dotenv = require('dotenv').config();
const cors = require('cors');
const {ChecarCampos} = require('./middleware/Validacao.js');
const router = require('./router/router.js');


app.use(cors());
app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use(router);
app.use(ChecarCampos);


app.listen(port,(err)=>{
    if(err){
        console.error(err.message)     
    }
    console.log(`http://localhost:${port}`);
})


