const express = require("express");
const router = express.Router();
const { validate, ValidationError, Joi } = require("express-validation");
const { Validacao } = require("../middleware/Validacao");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const banco = require("../banco/banco.js");
const TokenConfig = require("../token/TokenConfig.js");

//rota protegida por token
router.get("/users", TokenConfig.verificarToken, (req, res) => {
  console.log(req.name);
  res.status(200).json(banco);
});

//rota para cadastro de usuario
router.post("/cadastro", validate(Validacao, {}, {}), async (req, res) => {
  req.body.password = await bcrypt.hashSync(req.body.password, saltRounds);
  await banco.push(req.body);
  res.status(201).json(banco);
});

//rota para login de usuario
router.post("/login", validate(Validacao, {}, {}), async (req, res) => {
  var dados = await banco.find((x) => {
    return x.name == req.body.name && x.email == req.body.email;
  });

  if(dados){

  bcrypt.compare(req.body.password, dados.password).then((result) => {
    if (result) {
      res.status(200).json({
        status: true,
        token: TokenConfig.gerarToken(dados.name, dados.email),
      });
    }else{
      res.status(401).json({ status: false });  
    }
  });

  } else {
    res.status(500).json({ status: false });
  }

});

module.exports = router;
