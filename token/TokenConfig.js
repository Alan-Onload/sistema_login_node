const jwt = require("jsonwebtoken");
const dotenv = require("dotenv").config();

class TokenConfig {
  gerarToken = (name,email) => {
    return jwt.sign({ name,email },process.env.KEY,{ expiresIn: "1h" }) 
  };

  verificarToken = (req, res, next) => {
    const token = req.headers["authorization"].split(" ")[1];
    try {
      jwt.verify(token, process.env.KEY, (err, decode) => {
        if (err) console.error(`Erro: ${err.message}`);
        req.name = decode.name;
        
        next();
      });
    } catch (error) {
      res.status(401).json({ msg: false, token: null });
    }
  };
}

module.exports = new TokenConfig();
